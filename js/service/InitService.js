/*jslint browser: true*/
/*global console, Framework7, Movies, $document*/

Movies.angular.factory('InitService', ['$document', function ($document) {
  'use strict';

  var pub = {},
    eventListeners = {
      'ready' : []
    };
  
  pub.addEventListener = function (eventName, listener) {
    eventListeners[eventName].push(listener);
  };

  Movies.fw7.app.showIndicator();
  

  function onReady() {
   var fw7 = Movies.fw7,
     i;	 
	 
   fw7.views.push(fw7.app.addView('.view-main', fw7.options));	
   
		
    for (i = 0; i < eventListeners.ready.length; i = i + 1) {
      eventListeners.ready[i]();
    }
		
	
	// Gestion Back Button Android
	document.addEventListener("backbutton", function(e){
		var page=fw7.app.getCurrentView().activePage;
		fw7.app.hidePreloader();
		if(page.name=="index"){
			e.preventDefault();
			if(confirm("Voulez-vous quitter l'application?")){
				navigator.app.clearHistory();
				navigator.app.exitApp();
			}
		}
		else {
			fw7.app.mainView.router.back();
		}
	}, false);
	
			
	
  }
  
  
  // Init
  (function () {
    $document.ready(function () {	

      if (document.URL.indexOf("http://") === -1 && document.URL.indexOf("https://") === -1) {
        // Cordova
        console.log("Using Cordova/PhoneGap setting");
        document.addEventListener("deviceready", onReady, false);
		 function onDeviceReady() {
			  navigator.splashscreen.show();
		}
		
      } else {
        // Web browser
        console.log("Using web browser setting");
        onReady(); 			
      }
      
    });
  }());

  return pub;
  
}]);