/*jslint browser: true*/
/*global console, Framework7, Movies*/

Movies.angular.factory('DataService', ['$http', function ($http) {
  'use strict';

  var pub = {},
    eventListeners = {
      'movieClicked' : []
    };
  
  pub.addEventListener = function (eventName, callback) {
    eventListeners[eventName].push(callback);
  };
  
  pub.movieClicked = function (concert) {
    for (var i=0; i<eventListeners.movieClicked.length; i++) {
      eventListeners.movieClicked[i](concert);
    }
  };
  
  pub.getEvents = function () {
	return $http.jsonp('https://www.kimonolabs.com/api/e9itcxt6?apikey=e4bea2d86c33e6c642415f5e1794afb5&callback=JSON_CALLBACK', { cache: true, timeout: 5000 });	
  };
  
  pub.getGroupes = function () {	
	return $http.jsonp("https://www.kimonolabs.com/api/9dn7df4g?apikey=e4bea2d86c33e6c642415f5e1794afb5&callback=JSON_CALLBACK", { cache: true, timeout: 5000 });	
  };
  
  pub.getImages = function () {
	return $http.jsonp("https://www.kimonolabs.com/api/a51djab6?apikey=e4bea2d86c33e6c642415f5e1794afb5&callback=JSON_CALLBACK", { cache: true, timeout: 5000 });	
  };

  
  return pub;
  
}]);