/*jslint browser: true*/
/*global console, Movies, angular, Framework7*/

// Init angular
var Movies = {};
var $$ = Dom7;

Movies.config = {
};

Movies.angular = angular.module('Movies', []);

Movies.fw7 = {
  app : new Framework7({
    animateNavBackIcon: true
  }),
  options : {
    dynamicNavbar: true,
    domCache: true
  },
  views : []
};
/*jslint browser: true*/
/*global console, Framework7, Movies, $document*/

Movies.angular.factory('InitService', ['$document', function ($document) {
  'use strict';

  var pub = {},
    eventListeners = {
      'ready' : []
    };
  
  pub.addEventListener = function (eventName, listener) {
    eventListeners[eventName].push(listener);
  };

  function onReady() {
    var fw7 = Movies.fw7,
      i;

    fw7.views.push(fw7.app.addView('.view-main', fw7.options));
	
    
    for (i = 0; i < eventListeners.ready.length; i = i + 1) {
      eventListeners.ready[i]();
    }
  }
  
  
  // Init
  (function () {
    $document.ready(function () {

      if (document.URL.indexOf("http://") === -1 && document.URL.indexOf("https://") === -1) {
        // Cordova
        console.log("Using Cordova/PhoneGap setting");
        document.addEventListener("deviceready", onReady, false);
		 function onDeviceReady() {
			navigator.splashscreen.show();
		}
      } else {
        // Web browser
        console.log("Using web browser setting");
        onReady(); 			
      }
      
    });
  }());

  return pub;
  
}]);

/*jslint browser: true*/
/*global console, Framework7, Movies*/

Movies.angular.factory('DataService', ['$http', function ($http) {
  'use strict';

  var pub = {},
    eventListeners = {
      'movieClicked' : []
    };
  
  pub.addEventListener = function (eventName, callback) {
    eventListeners[eventName].push(callback);
  };
  
  pub.movieClicked = function (movie) {
    for (var i=0; i<eventListeners.movieClicked.length; i++) {
      eventListeners.movieClicked[i](movie);
    }
  };
  
  pub.getEvents = function () {
	return $http.jsonp('https://www.kimonolabs.com/api/e9itcxt6?apikey=e4bea2d86c33e6c642415f5e1794afb5&callback=JSON_CALLBACK');	
  };
  
  pub.getGroupes = function () {	
	return $http.jsonp("https://www.kimonolabs.com/api/9dn7df4g?apikey=e4bea2d86c33e6c642415f5e1794afb5&callback=JSON_CALLBACK");
  };
  
  pub.getImages = function () {
	return $http.jsonp("https://www.kimonolabs.com/api/a51djab6?apikey=e4bea2d86c33e6c642415f5e1794afb5&callback=JSON_CALLBACK");
  };
  
  return pub;
  
}]);

Movies.angular.factory('myCache', function($cacheFactory) {
 return $cacheFactory('myData');
});
Movies.angular.factory('ImgCache', function($cacheFactory) {
 return $cacheFactory('myImgCache');
});

/*jslint browser: true*/
/*global console, Movies*/

Movies.angular.controller('IndexPageController', ['$scope', '$http', 'InitService', 'DataService', 'myCache', 'ImgCache', function ($scope, $http, InitService, DataService, myCache, ImgCache) {
  'use strict';
  
	$scope.onItemClicked = function (movie) {
		DataService.movieClicked(movie);
	}
	
	// LISTE CONCERT
	InitService.addEventListener('ready', function () {
	
		$scope.loading = true;
		console.log('loading on');
		DataService.getEvents('event').then(function (result) {				
			$scope.movies = result.data['results']['collection1'];
			console.log('event load');
			$scope.loading = false;		
		}, function (err) {
			console.error(err);
	});
	
	// DETAL CONCERT
	var cache = myCache.get('myData');
	if (cache) {
		$scope.groupes = cache;
		console.log('cache on');
	}
	else {		
		DataService.getGroupes('detail').then(function (result) {
			$scope.groupes = result.data['results']['collection1'];		
			myCache.put('myData', result.data['results']['collection1']);
			}, function (err) {
			  console.error(err);
		});
	}    
	
	 //IMAGE
	DataService.getImages('image').then(function (result) {
		$scope.image = result.data['results']['collection1'];
		ImgCache.put('myImgCache', result.data['results']['collection1']);		
		}, function (err) {
		  console.error(err);
	});		
	
  });
    
}]);

/*jslint browser: true*/
/*global console, Movies*/

Movies.angular.controller('DetailPageController', ['$scope', '$http', 'InitService', 'DataService', 'myCache', 'ImgCache', function ($scope, $http, InitService, DataService,  myCache, ImgCache) {
  'use strict';
  
  DataService.addEventListener('movieClicked', function (movie) {  
	$scope.movie = movie;
	var cache = myCache.get('myData');
	if (cache) {
	 $scope.groupes = cache;
	}
	else {
		DataService.getGroupes('detail').then(function (result) {
			$scope.groupes = result.data['results']['collection1'];		
			myCache.put('myData', result.data['results']['collection1']);
			}, function (err) {
			  console.error(err);
		});
	} 	
  });
  
}]);