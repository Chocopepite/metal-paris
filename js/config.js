/*jslint browser: true*/
/*global console, Movies, angular, Framework7*/

// Init angular
var Movies = {};
var $$ = Dom7;

Movies.config = {
};

Movies.angular = angular.module('Movies', [])
.filter('custom', function() {
  // Filtre NG-Repeat custom sur les noms des groupes
  return function(input, search) {
    if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
      var actual = ('' + value.groupe_1 + value.groupe_2 + value.groupe_3 + value.groupe_4).toLowerCase();
      if (actual.indexOf(expected) !== -1) {
        result[key] = value;	
      }
    });
    return result;
  }
})
.filter('custom2', function() {
  // Filtre NG-Repeat custom sur les noms des groupes
  return function(input, search) {
    if (!input) return input;
    if (!search) return input;
    var expected = ('' + search).toLowerCase();
    var result = {};
    angular.forEach(input, function(value, key) {
      var actual = ('' + value.departement ).toLowerCase();
      if (actual.indexOf(expected) !== -1) {
        result[key] = value;	
      }
    });
    return result;
  }
});

Movies.fw7 = {
  app : new Framework7({   
	material: true,	
  }),
  options : {
    domCache: true,
  },
  views : []
};

