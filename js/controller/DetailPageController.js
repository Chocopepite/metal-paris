/*jslint browser: true*/
/*global console, Movies*/

Movies.angular.controller('DetailPageController', ['$scope', '$http', 'InitService', 'DataService', function ($scope, $http, InitService, DataService) {
  'use strict';
  
	DataService.addEventListener('movieClicked', function (concert) {  
		$scope.concert = concert;
		
		// LOAD IMAGE
		DataService.getImages('image').then(function (result) {
			$scope.image = result.data['results']['collection1'];				
			}, function (err) {
			  console.error(err);
		});	 
			
		
		// AJOUT FAVORIS
		$scope.AddFav = function (url) {
			// Si l'event est nouveau, on décremente le compteur du badge Nouveaux
			var isnew = localStorage.getItem(url);
			if (isnew === "new"){
				angular.element(document.getElementById('TabController')).scope().CountNew = angular.element(document.getElementById('TabController')).scope().CountNew - 1;
			}
			var add = localStorage.setItem(url, "1");
			angular.element(document.getElementById('TabController')).scope().CountFav = angular.element(document.getElementById('TabController')).scope().CountFav + 1;
		};
		
		// DELETE FAVORIS
		$scope.DeleteFav = function (url) {
			var del = localStorage.removeItem(url);
			angular.element(document.getElementById('TabController')).scope().CountFav = angular.element(document.getElementById('TabController')).scope().CountFav - 1;
		};
		
		// TEST FAVORIS
		$scope.IsFav = function (url) {
			var fav = localStorage.getItem(url);
			if (fav === "1"){
				return 1;
			};
		};
		
		// TEST NEW
		$scope.IsNew = function (url) {
			var isnew = localStorage.getItem(url);
			if (isnew === "new"){
				return 1;
			};
		};
		
	});	
	

}]);