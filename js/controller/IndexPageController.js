/*jslint browser: true*/
/*global console, Movies*/

Movies.angular.controller('IndexPageController', ['$scope', '$http', '$filter', 'InitService', 'DataService', '$timeout',    function ($scope, $http, $filter, InitService, DataService, $timeout) {
  'use strict';
 		
	$scope.onItemClicked = function (concert) {
		DataService.movieClicked(concert);
	}
	var ListEvent = '';
	var DetailEvent = '';
	var version = localStorage.getItem('version');
	var json = localStorage.getItem('json');
	var json = ('json: ', JSON.parse(json));
	var ListVersion = '';
	var OffLine = false;
	var LocalDepartement = localStorage.getItem("departement");
	$scope.SearchNull = 0;
	$scope.DepNull = 0;
	
	$scope.LocalDepartement = LocalDepartement;
	
	Movies.fw7.app.onPageAfterAnimation('index', function(page){		
			if (Object.keys($scope.filteritem).length === 0){
				$scope.DepNull = 1;					
			}
			else{
				$scope.DepNull = 0;
				$scope.SearchNull = 0;
			}
			$scope.$apply();
	});
		
	InitService.addEventListener('ready', function () {	
			
			if (LocalDepartement != ''){
				$scope.InfoDep = "dans le "+LocalDepartement;
			}
			else{
				$scope.InfoDep = '';
			}
			
			
			// LISTE EVENT
			DataService.getEvents('event').then(function (result) {
				ListEvent = result.data['results']['collection1'];	
				ListVersion = result.data['version'];					
				MergeJson();
			}, function (err) {
				ListEvent = "offline";
				MergeJson();
			});
			
			
			// DETAIL CONCERT
			DataService.getGroupes('detail').then(function (result) {				
				DetailEvent = result.data['results']['collection1'];
				MergeJson();
			}, function (err) {
				 DetailEvent = "offline";
				 MergeJson();
			});	
	
		
		// AJOUT FAVORIS
		$scope.AddFav = function (url) {
			var isnew = localStorage.getItem(url);
			if (isnew === "new"){
				angular.element(document.getElementById('TabController')).scope().CountNew = angular.element(document.getElementById('TabController')).scope().CountNew - 1;
			}
			var add = localStorage.setItem(url, "1");
			angular.element(document.getElementById('TabController')).scope().CountFav = angular.element(document.getElementById('TabController')).scope().CountFav + 1;
		};
		
		// SUPPRESSION FAVORIS
		$scope.DeleteFav = function (url) {		 
			var del = localStorage.removeItem(url);
			angular.element(document.getElementById('TabController')).scope().CountFav = angular.element(document.getElementById('TabController')).scope().CountFav - 1;
		};
		
		// TEST FAVORIS
		$scope.IsFav = function (url) {
			var fav = localStorage.getItem(url);
			if (fav === "1"){
				return 1;
			};
		};
		
		// TEST NEW
		$scope.IsNew = function (url) {
			var isnew = localStorage.getItem(url);
			if (isnew === "new"){
				return 1;
			};
		};
		// TEST NEW in local
		$scope.NewInLocal = function () {
			for ( var i = 0, len = localStorage.length; i <= len; ++i ) {
				var LocalValue = localStorage.getItem( localStorage.key( i ) ) ;
				var LocalKey = localStorage.key(i);
				if (LocalValue === "new"){						
					return true;
				}
			}
		};
		
		// TEST FAV in local
		$scope.FavInLocal = function () {
			for ( var i = 0, len = localStorage.length; i <= len; ++i ) {
				var LocalValue = localStorage.getItem( localStorage.key( i ) ) ;
				var LocalKey = localStorage.key(i);
				if (LocalValue === "1"){						
					return true;
				}
			}
		};
		
		// Bouton Suppression des Nouveaux Events
		$scope.DeleteNew = function(){
			for ( var i = 0, len = localStorage.length; i <= len; ++i ) {
				var LocalValue = localStorage.getItem( localStorage.key( i ) ) ;
				var LocalKey = localStorage.key(i);	
				if ( LocalKey != 'json'){
					if (LocalValue === "new"){						
						localStorage.removeItem(LocalKey);
						$scope.DeleteNew();
					}
				}
			}
			angular.element(document.getElementById('TabController')).scope().CountNew = 0;
			angular.element(document.getElementById('IndexPage')).scope().filtreNew = "0";
			angular.element(document.getElementById('TabController')).scope().buttonNew = 0 ;			
		};
		
   });
   
	var count = 0;
	var countnew = 0;
   // FUSION JSON POUR AFFICHAGE
   var MergeJson = function(){
		
		// Mode Offline
		if (DetailEvent === 'offline' || ListEvent === 'offline'){
			if (OffLine === false){
				OffLine = true;
				Movies.fw7.app.hideIndicator();
				if (json != null){
					alert('Connexion impossible, mode offline');
					$scope.concerts = json;	
					angular.element(document.getElementById('TabController')).scope().preload2 = 1;
					
					// MAJ COMPTEUR FAV & NEW OFFLINE
					for ( var i = 0, len = localStorage.length; i < len; ++i ) {
						var LocalValue = localStorage.getItem( localStorage.key( i ) ) ;
						var LocalKey = localStorage.key(i);
						if (LocalValue === "1"){
							 count = count + 1;
						}
						if (LocalValue === "new"){	
							 countnew = countnew + 1;
						}					
					}
					angular.element(document.getElementById('TabController')).scope().CountFav = count;
					angular.element(document.getElementById('TabController')).scope().CountNew = countnew;
				}
				else{
					alert('Connexion impossible');
				}
			}
		}
		
		else if ((DetailEvent != '' && ListEvent != '') && (DetailEvent != 'offline' && ListEvent != 'offline')){			
			// On retire l'event s'il est annulé
			angular.forEach(ListEvent, function(value1, key1) {
				if (value1.groupe_1 === undefined){
					ListEvent.splice(value1.index - 1, 1);
				}
			});
			
			
			// MAJ COMPTEUR FAV & NEW ONLINE
			for ( var i = 0, len = localStorage.length; i <= len; ++i ) {
				var LocalValue = localStorage.getItem( localStorage.key( i ) ) ;
				var LocalKey = localStorage.key(i);
				if (LocalValue === "1"){
					var found = $filter('filter')(ListEvent, {url: LocalKey}, true);
					if (found.length) {
					 count = count + 1;
					//console.log(LocalKey + " favoris");
					} else {
						localStorage.removeItem(LocalKey);
						console.log(LocalKey +' remove fav');
					}
				}
				if (LocalValue === "new"){
					var found = $filter('filter')(ListEvent, {url: LocalKey}, true);
					if (found.length) {
					 countnew = countnew + 1;
					} else {
						localStorage.removeItem(LocalKey);
						console.log(LocalKey + ' remove new');
					}	
				}					
			}
			// Mise a jour du badge "Nombre de Favoris"
			angular.element(document.getElementById('TabController')).scope().CountFav = count;
			// Mise a jour du badge "Nombre de Nouveaux events"
			angular.element(document.getElementById('TabController')).scope().CountNew = countnew;
			
			var object = angular.merge({}, ListEvent, DetailEvent);	
			
			Movies.fw7.app.hideIndicator();
			$scope.concerts = object;			
			
			
			// Recherche nulle 
			$scope.$watch("search", function(query){
				if (Object.keys($scope.filteritem).length === 0){					
					$scope.SearchNull = 1;
				}
				else{
					$scope.SearchNull = 0;
				}
			});
			
			
			
			angular.element(document.getElementById('TabController')).scope().preload2 = 1;
			
			// Mise a jour JSON si version serveur est plus récente
			if (ListVersion != version){
				localStorage.setItem("version", ListVersion);				
				// Si on a un json stocké en local => on recherche les nouveaux events
				if (json != null){
					FindNew(object);
				}
				else{
					var add = localStorage.setItem("json", JSON.stringify(object));
				}
			}		
		}		
		
	};
	
	// Recherche des Nouveaux events
	var FindNew = function(object){
		var dep = localStorage.getItem("departement");
		angular.forEach(object, function(value1, key1) {
			var keepGoing = true;
			angular.forEach(json, function(value2, key2) {
				if(keepGoing) {
					if (value1.url === value2.url){
						 keepGoing = false;
					}
				}			
			 });
			 if (keepGoing === true){
				if (value1.departement === dep){
					var add = localStorage.setItem(value1.url, 'new');
					angular.element(document.getElementById('TabController')).scope().CountNew = angular.element(document.getElementById('TabController')).scope().CountNew + 1;
				}
			 }
		});		
		var add = localStorage.setItem("json", JSON.stringify(object));	
		
	};
	
	

   
    
}]);