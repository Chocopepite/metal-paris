/*jslint browser: true*/
/*global console, Movies*/

Movies.angular.controller('HomeController', ['$scope', '$http', 'InitService', 'DataService', function ($scope, $http, InitService, DataService) {

	$scope.Home = function(){
		angular.element(document.getElementById('TabController')).scope().homevar = 1;
		angular.element(document.getElementById('IndexPage')).scope().TITRE = "Liste des concerts";
		Movies.fw7.app.mainView.router.loadPage('#index');
	};
	
	$scope.HomeFav = function(){
		angular.element(document.getElementById('IndexPage')).scope().filtreFav = "1";
		angular.element(document.getElementById('TabController')).scope().buttonFav = 1;
		angular.element(document.getElementById('TabController')).scope().homevar = 1;
		angular.element(document.getElementById('IndexPage')).scope().TITRE = "Vos Favoris";
		angular.element(document.getElementById('IndexPage')).scope().InfoDep = "";
		Movies.fw7.app.mainView.router.loadPage('#index');
		
	}
	
	$scope.HomeNew = function(){
		angular.element(document.getElementById('IndexPage')).scope().filtreNew = "1";
		angular.element(document.getElementById('TabController')).scope().buttonNew = 1;
		angular.element(document.getElementById('TabController')).scope().homevar = 1;
		angular.element(document.getElementById('IndexPage')).scope().TITRE = "Nouveaux concerts";
		angular.element(document.getElementById('IndexPage')).scope().InfoDep = "";
		Movies.fw7.app.mainView.router.loadPage('#index');
	}
	

}]);