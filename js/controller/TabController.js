/*jslint browser: true*/
/*global console, Movies*/

Movies.angular.controller('TabController', ['$scope', '$http', 'InitService', 'DataService', '$filter', function ($scope, $http, InitService, DataService, $filter) {
  'use strict';
	
	// Bouton Favoris: filtre le scope sur les Favoris et retire le filtre sur les nouveautés
	$scope.ShowFav = function (){
		var LocalDepartement = localStorage.getItem("departement");		
		var NavFav = angular.element(document.getElementById('IndexPage')).scope().filtreFav;
		
		if (NavFav === "1" || NavFav === "undefined"){					
			angular.element(document.getElementById('IndexPage')).scope().LocalDepartement = LocalDepartement;
			angular.element(document.getElementById('IndexPage')).scope().filtreFav = "0";
			angular.element(document.getElementById('IndexPage')).scope().TITRE = "Liste des concerts";			
			angular.element(document.getElementById('IndexPage')).scope().InfoDep = "dans le " + LocalDepartement;			
		}
		else{
			angular.element(document.getElementById('IndexPage')).scope().filtreNew = "0";	
			angular.element(document.getElementById('IndexPage')).scope().filtreFav = "1";	
			angular.element(document.getElementById('IndexPage')).scope().LocalDepartement = "";
			angular.element(document.getElementById('IndexPage')).scope().TITRE = "Vos Favoris";
			angular.element(document.getElementById('IndexPage')).scope().InfoDep = "";
		}
	};
	
	// Initialisation de la barre de recherche
	var mySearchbar = Movies.fw7.app.searchbar('.searchbar', {
		searchList: '.list-block-search',
		searchIn: '.item-title'
	});  
	
	// Bouton Rechercher: Affiche la barre de recherche avec un padding top
	$scope.ShowSearch = function (){
		var NavSearch = angular.element(document.getElementById('IndexPage')).scope().showSearch;
		var myElement = document.querySelector(".navbar-fixed .page>.searchbar~.page-content, .navbar-fixed>.searchbar~.page-content, .navbar-through .page>.searchbar~.page-content, .navbar-through>.searchbar~.page-content");
		if (NavSearch === "1" || NavSearch === "undefined"){
			myElement.style.padding = "60px 0";
			angular.element(document.getElementById('IndexPage')).scope().showSearch = "0";		
			angular.element(document.getElementById('IndexPage')).scope().search = '';
		}
		else{			
			myElement.style.padding = "100px 0";
			angular.element(document.getElementById('IndexPage')).scope().showSearch = "1";	
		}	
	};
	
	// Bouton Nouveauté: filtre le scope sur les Nouveaux events et retire le filtre sur les Favoris
	$scope.ShowNew = function (){
		var NavNew = angular.element(document.getElementById('IndexPage')).scope().filtreNew;
		var LocalDepartement = localStorage.getItem("departement");
		if (NavNew === "1" || NavNew === "undefined"){	
			angular.element(document.getElementById('IndexPage')).scope().LocalDepartement = LocalDepartement;
			angular.element(document.getElementById('IndexPage')).scope().filtreNew = "0";
			angular.element(document.getElementById('IndexPage')).scope().TITRE = "Liste des concerts";
		}
		else{
			angular.element(document.getElementById('IndexPage')).scope().filtreFav = "0";			
			angular.element(document.getElementById('IndexPage')).scope().LocalDepartement = "";
			angular.element(document.getElementById('IndexPage')).scope().filtreNew = "1";
			angular.element(document.getElementById('IndexPage')).scope().TITRE = "Nouveaux concerts";
			angular.element(document.getElementById('IndexPage')).scope().InfoDep = "";			
		}	
	};
	

}]);